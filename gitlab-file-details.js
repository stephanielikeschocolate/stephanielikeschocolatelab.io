#!/usr/bin/env node

(async function main() {
	const { ArgumentParser } = require('argparse');
	const { Gitlab } = require('gitlab');
	const package = require('./package');

	const parser = new ArgumentParser({
		version: package.version,
		addHelp: true
	});
	parser.addArgument('file', {
		help: 'File to fetch GitLab details for.',
		action: 'store'
	});
	parser.addArgument(['-g', '--gitlab-host'], {
		help: 'GitLab host.',
		defaultValue: 'https://gitlab.com',
		action: 'store',
		dest: 'gitlabHost'
	});
	parser.addArgument(['-p', '--project-id'], {
		help: 'GitLab project ID.',
		action: 'store',
		dest: 'projectId'
	});
	parser.addArgument(['-r', '--ref'], {
		help: 'GitLab ref to use to fetch file details.',
		defaultValue: 'master',
		action: 'store',
		dest: 'ref'
	});
	parser.addArgument(['-t', '--token'], {
		help: 'GitLab personal access token.',
		action: 'store',
		dest: 'gitlabToken'
	});

	const args = parser.parseArgs();

	const api = new Gitlab({
		host: args.gitlabHost,
		token: args.gitlabToken
	});

	const response = {};
	try {
		response.file = await api.RepositoryFiles.show(args.projectId, args.file, args.ref);
		response.commit = await api.Commits.show(args.projectId, response.file.last_commit_id);
		response.user = await api.Users.search(response.commit.author_email);
	} catch (e) {
		console.error(e);
		process.exit(e.errno);
	}
	console.log(`export FILE_AUTHOR_USERNAME="${response.user[0].username}"`);
	console.log(`export FILE_AUTHOR_AVATAR_URL="${response.user[0].avatar_url}"`);
	console.log(`export FILE_AUTHOR_WEB_URL="${response.user[0].web_url}"`);
	console.log(`export FILE_AUTHOR_DATE="${new Date(response.commit.authored_date).toUTCString()}"`);
	process.exit(0);
})();
