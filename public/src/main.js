function getURLParameters() {
	return Object.assign({}, getURLQueryParameters(), getURLHashParameters());
};

function getURLQueryParameters() {
	return getEncodedParameters(window.location.search);
};

function getURLHashParameters() {
	return getEncodedParameters(window.location.hash.substring(1));
};

function getEncodedParameters(encodedString) {
	let params = {};
	for (let p of new URLSearchParams(encodedString)) {
		params[p[0]] = p[1];
	}
	return params;
};

function getURLParameter(name) {
	return getURLQueryParameter(name) || getURLHashParameter(name);
};

function getURLQueryParameter(name) {
	return getEncodedParameter(window.location.search, name);
};

function getURLHashParameter(name) {
	return getEncodedParameter(window.location.hash, name);
};

function getEncodedParameter(encodedString, parameterName) {
	return decodeURIComponent((new RegExp('[?|#|&]' + parameterName + '=' + '([^&;]+?)(&|#|;|$)').exec(encodedString) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

var disqus_config = function () {
  this.page.url = window.location.href;
  this.page.identifier = window.location.pathname;
};
