# Hello, World!
<p class="align-middle">
	<a href="${FILE_AUTHOR_WEB_URL}" style="text-decoration: none">
		<img src="${FILE_AUTHOR_AVATAR_URL}" width="24" height="24" class="d-inline-block align-middle" alt>
	</a>
	<a href="${FILE_AUTHOR_WEB_URL}">${FILE_AUTHOR_USERNAME}</a>
	on ${FILE_AUTHOR_DATE}
</p>

---

This is the first of `stephanielikeschocolate`'s blog posts.

Stay tuned for more!
