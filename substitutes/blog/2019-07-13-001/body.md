# The Voice of `stephanielikeschocolate`
<p class="align-middle">
	<a href="${FILE_AUTHOR_WEB_URL}" style="text-decoration: none">
		<img src="${FILE_AUTHOR_AVATAR_URL}" width="24" height="24" class="d-inline-block align-middle" alt>
	</a>
	<a href="${FILE_AUTHOR_WEB_URL}">${FILE_AUTHOR_USERNAME}</a>
	on ${FILE_AUTHOR_DATE}
</p>

---

Who *is* `stephanielikeschocolate`? *What* is `stephanielikeschocolate`? And
just how much does `stephanielikeschocolate` actually *like* chocolate?

Over the course of time, many have speculated that `stephanielikeschocolate` is
a cosmic entity gifted with the second sight and the power to heal. Eye
witnesses have testified that in an era long past, it is likely that
`stephanielikeschocolate` had to have been bestowed with the emperor's gauntlet
— that is to say that `stephanielikeschocolate` is undoubtedly capable of
conquering and controlling the will of mankind.

With a might so vast and a disposition so benevolent, some have even likened
`stephanielikeschocolate` to the divine creator of all things in the universe,
the great puppet master pulling the strings behind the scenes to tell a story of
astrological magnitude, and a miracle worker swooping down to the realm of men
to grant blessings to those of purest heart.

It is without doubt that `stephanielikeschocolate` is a true wonder to us all...
But really though I'm just a person. :grin:

I'm a person with a lot of thoughts and a lot of feelings, much like any other
person out there. But of course, I'm not special! Why would I be? Like anybody
else, sure, I do have periods of positive mental health where things seem to be
going my way in life, I feel happy, I feel confident, I feel like I'm
succeeding, and I'm treating my body well to support all of this. And yet, also
like anybody else, I too periodically slip into a dark, dark place where my own
thoughts consume all of the good that I may have once had in life and a swirling
vortex of hopelessness, uselessness, valuelessness, and all of their friends and
family starts to bubble up from within me and drag me further into the depths of
my own cold heart, spreading like a frost bite from within me to ultimately
manifest its nastiness in the real world by controlling my very own actions and
interactions around other human beings until finally by sheer chance, or perhaps
the grace of a higher being touching down to purify my cursed mind, I am able to
pull free from that kind of torment and take another shot at living a pleasant
life for as long as I can manage it.

Sometimes I'm a mess. Evidently, I created this entire blog site in one
night[^1] while my mind was in that tortured hellish state that I just
described. As it happens, I am writing this very post while sitting on the
toilet! That is not to say I am feeling depressed or anything at this moment,
but it does indicate I can still be a mess even in a good state of mind. I think
the act of writing though — whether it be code, literature, or a simple blog
post — is good therapy for the mind. It's stimulating and I think promotes
keeping the mind sharp and witty. It gives you the opportunity to think
critically about a topic, practise your vocabulary and grammar, and challenge
yourself by pushing the limits of your writing style.

[^1]: The current state of the blog did not take me just one night to
accomplish, however it only took one night to get it into working shape. I plan
to discuss how I went about making it all in a future post, as long as I can
make that kind of a blog post sound interesting enough.

Writing style... For me it's always been about writing style. I like to get
weird with it. Right from the beginning of this read, you could probably already
tell that about me. A lot of the time I hold myself back from writing anything
because I don't feel like I have expertise on the matter. That's why in this
blog, I'm going to forcefully reject that entire concept and write about
anything that I want regardless of how correct or authoritative I actually am on
the matter. I want to do this because I want to put my voice out there. For some
reason, I really want my thoughts to be read by others, maybe so that I can
connect with others, maybe for self therapy, or maybe so that I may even inspire
others. Sometimes I may write in the tone of my own voice, like
this. Sometimes I may write as though I was trying to tell an epic tale, using
diction I wouldn't normally attempt to use in casual conversation. Sometimes I
may try to be matter-of-fact about a topic. Regardless of how I choose to write,
what's important is the decision to write in the first place.

I'm using this as a platform to share my thoughts and my voice. Whatever I end
up writing about will be in my voice. Whatever style I decide to use to write on
any particular entry will still be my voice. My name is Stephanie Reynolds, but
you can call me `stephanielikeschocolate`.

This is the voice of `stephanielikeschocolate`.
