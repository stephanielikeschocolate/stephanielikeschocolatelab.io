#!/usr/bin/env sh

set -x

export TEMP_DIR=$(mktemp -d)

ls components | (while read f; do
  export ${f%.*}="$(cat components/${f})"
done

find substitutes -type d | while read d; do
  if [ -f "${d}/.template" ] && [ -f "${d}/body.md" ] && [ -f "${d}/head.md" ]; then
    ENV_FILE="${TEMP_DIR}/${d/substitutes/views}.html.env"
    mkdir -p "$(dirname ${ENV_FILE})"
    ./gitlab-file-details.js -p ${CI_PROJECT_ID} -t ${GITLAB_API_TOKEN} -r ${CI_COMMIT_REF_NAME} "${d}/body.md" > "${ENV_FILE}"
    mkdir -p "views/${d#substitutes/}"
    TEMPLATE_HEAD="$(./md.js ${d}/head.md)" TEMPLATE_BODY="$(./md.js ${d}/body.md)" envsubst < "$(cat ${d}/.template)" > "${d/substitutes/views}.html"
  fi
done

find views -type f | while read f; do
  f="${f}" sh -c '
    OUT_FILE="public/${f#*/}"
    mkdir -p "$(dirname ${OUT_FILE})"
    if [ -f "${TEMP_DIR}/${f}.env" ]; then
      source "${TEMP_DIR}/${f}.env"
    fi
    envsubst < "${f}" > "${OUT_FILE}"
  '
done
)

rm -rf "${TEMP_DIR}"
