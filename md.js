#!/usr/bin/env node

const argparse = require('argparse');
const fs = require('fs');
const md = require('markdown-it')('commonmark')
	.use(require('markdown-it-emoji'))
	.use(require('markdown-it-footnote'));
const package = require('./package');

const parser = new argparse.ArgumentParser({
	version: package.version,
	addHelp: true
});
parser.addArgument('file', {
	help: 'File to read.',
	action: 'store'
});

const args = parser.parseArgs();

try {
	console.log(md.render(fs.readFileSync(args.file).toString()));
} catch (e) {
	console.error(e.message);
	process.exit(e.errno);
}
